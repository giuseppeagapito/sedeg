# Select Essential Differential Expressed Genes SEDEG



## Description
Select Essential Differential Expressed Genes (SEDEG), a parallel software pipeline to automatically preprocess, filter and select relevant DEGs to perform PEA.

SEDEG is implemented in Python and provides the following automated actions executed in parallel: \textit{i)} splitting  DEGs list in down- and up-regulated gene groups; \textit{ii)} building the similarity matrices related to both DEGs groups; \textit{iii)} mapping of both similarity matrices in networks; \textit{iv)} PEA using the identified essential DEGs from both groups. Preliminary results shown that SEDEG identifies essential DEGs, generating in a few time a list of specific relevant enriched pathway for the condition under-investigation; that otherwise would require several hours of manual work and several different scripts.
SEDEG pipeline contributes to speed up and improve the DEGs consolidation process necessary to compute PEA, automating several steps currently performed manually. In addition, the DEGs consolidation provides more peculiar lists of DEG to perform PEA, contributing to improve the relevance and significance of the enriched pathways.


## Installation
Make sure to install and import igraph. igraph is primarily a library written in C. Therefore, if you would like to use igraph’s functionality in Python, you must install a few packages. 
https://igraph.org/python/tutorial/0.9.8/install.html

## Usage
Run SEDEG script using the following list of arguments. 
The first argument is the DEG data set path. The second argument is the path to the results folder and the last one is the path to the folder containing the iid database.

**python ./SEDEGmain.py "./GEOdataset.tsv" "./results" "./iiddb.txt"**



## License
SEDEG. Code licensed under [GNU GPL 2](http://www.gnu.org/licenses/gpl-2.0.html) or later, documentation under [GNU FDL](http://www.gnu.org/copyleft/fdl.html).

