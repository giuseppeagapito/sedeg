from graph_tool.all import *
import pandas as pd
import numpy as np
import csv
import time

map_str_in_int = {}
map_int_in_str = {}

# the key value is the synonym
map_synonyms = {}


def importInteractionDBFromFile(path):
    database = pd.read_csv(
        path,
        sep="\t", encoding='utf-8', engine='python', usecols=[2, 3], quoting=csv.QUOTE_NONE, header=None)
    df1 = database.replace(np.nan, '', regex=True)
    # save to file the reduced and cleanead dataset we use only gene name
    return df1


g = Graph()
g.set_directed(False)


def createNetwork(data_frame):
    print("network creation started")
    starttime = time.time()
    global g
    for index, row in data_frame.iterrows():
        if (';' not in row[2] and ';' not in row[3]):
            n1 = row[2].strip()
            n2 = row[3].strip()
            add_elements_to_graph(n1_str=n1, n2_str=n2, g=g)
        elif (';' in row[2] and ';' in row[3]):
            x = row[2].strip()
            x1 = x.split(';')
            y = row[3].strip()
            y1 = y.split(';')
            add_elements_to_graph(n1_str=x1[0].strip(), n2_str=y1[0].strip(), g=g)
            for i in range(1, len(x1)):
                map_synonyms[x1[i].strip()] = x1[0].strip()
            for j in range(len(y1)):
                map_synonyms[y1[j].strip()] = y1[0].strip()
        elif (';' in row[2]):
            x = row[2].strip()
            x1 = x.split(';')
            add_elements_to_graph(n1_str=x1[0].strip(), n2_str=row[3].strip(), g=g)
            for i in range(1, len(x1)):
                map_synonyms[x1[i].strip()] = x1[0].strip()
        elif (';' in row[3]):
            x = row[3].strip()
            x1 = x.split(';')
            add_elements_to_graph(n1_str=row[2].strip(), n2_str=x1[0].strip(), g=g)
            for i in range(1, len(x1)):
                map_synonyms[x1[i].strip()] = x1[0].strip()
    endtime = time.time()
    print("end graph creation.")
    print('end in ', (endtime - starttime))

    # print('vertici? ', g.get_vertices())
    # print('MED8' in map_str_in_int.keys())
    # print('DGKQ' in map_str_in_int.keys())

    # lista = get_proteins_target_neighbors(graph=g, prot='MED8')
    # print(len(lista))

def create_network_extend_genes_target(data_frame, genes_target_to_extend):
    print("network creation started")
    starttime = time.time()
    global g
    for index, row in data_frame.iterrows():
        if (';' not in row[2] and ';' not in row[3]):
            n1 = row[2].strip()
            n2 = row[3].strip()
            add_elements_to_graph(n1_str=n1, n2_str=n2, g=g)
        elif (';' in row[2] and ';' in row[3]):
            x = row[2].strip()
            x1 = x.split(';')
            y = row[3].strip()
            y1 = y.split(';')
            add_elements_to_graph(n1_str=x1[0].strip(), n2_str=y1[0].strip(), g=g)
            for i in range(1, len(x1)):
                map_synonyms[x1[i].strip()] = x1[0].strip()
            for j in range(len(y1)):
                map_synonyms[y1[j].strip()] = y1[0].strip()
        elif (';' in row[2]):
            x = row[2].strip()
            x1 = x.split(';')
            add_elements_to_graph(n1_str=x1[0].strip(), n2_str=row[3].strip(), g=g)
            for i in range(1, len(x1)):
                map_synonyms[x1[i].strip()] = x1[0].strip()
        elif (';' in row[3]):
            x = row[3].strip()
            x1 = x.split(';')
            add_elements_to_graph(n1_str=row[2].strip(), n2_str=x1[0].strip(), g=g)
            for i in range(1, len(x1)):
                map_synonyms[x1[i].strip()] = x1[0].strip()
    endtime = time.time()
    print("end graph creation.")
    print('end in ', (endtime - starttime))
    startgenetime = time.time()
    print('genes extension started')
    lista = get_proteins_target_neighbors(graph=g, l_prots=genes_target_to_extend)
    endextension = time.time()
    print('genes extension ended in ', endextension-startgenetime)
    return lista

def get_proteins_target_neighbors(graph, l_prots):
    neighbors = []
    for p in l_prots:
        # print(p['name'], ">>>>>>>>>>>>>>>>")
        if p['name'] in map_str_in_int.keys():
            v = map_str_in_int.get(p['name'])
            for w in graph.vertex(v).out_neighbors():
                name = map_int_in_str.get(graph.vertex_index[w])
                neighbors.append(name)
            # print(name)
        # else:
        #     print('not found search synonym after parsing.....')
    return neighbors


def get_proteintarget_neighbors(graph, prot):
    neighbors = []
    v = map_str_in_int.get(prot)
    # print(map_int_in_str.get(v), 'numero vicini: ', graph.vertex(v).out_degree())
    for w in graph.vertex(v).out_neighbors():
        name = map_int_in_str.get(graph.vertex_index[w])
        neighbors.append(name)
        # print(name)
    return neighbors


# add elements needs node1, node2, the nT nodeType, nID node id, nN node name and eT edgeType to be addet to the graph g
def add_elements_to_graph(n1_str, n2_str, g):
    if n1_str in map_str_in_int.keys() and n2_str in map_str_in_int.keys():
        g.add_edge(map_str_in_int[n1_str], map_str_in_int[n2_str])
    elif n1_str in map_str_in_int.keys() and n2_str not in map_str_in_int.keys():
        v2 = g.add_vertex()
        map_str_in_int[n2_str] = g.vertex_index[v2]
        map_int_in_str[g.vertex_index[v2]] = n2_str
        g.add_edge(map_str_in_int.get(n1_str), v2)
    elif n1_str not in map_str_in_int.keys() and n2_str in map_str_in_int.keys():
        v1 = g.add_vertex()
        map_str_in_int[n1_str] = g.vertex_index[v1]
        map_int_in_str[g.vertex_index[v1]] = n1_str
        g.add_edge(v1, map_str_in_int.get(n2_str))
    elif n1_str not in map_str_in_int.keys() and n2_str not in map_str_in_int.keys():
        v1 = g.add_vertex()
        map_str_in_int[n1_str] = g.vertex_index[v1]
        map_int_in_str[g.vertex_index[v1]] = n1_str
        v2 = g.add_vertex()
        map_str_in_int[n2_str] = g.vertex_index[v2]
        map_int_in_str[g.vertex_index[v2]] = n2_str
        g.add_edge(v1, v2)
    # print(g)


# given a vertex id the method returns the genename as string
def getgenename(id):
    return map_int_in_str(id)


# given a genename the method returns the vertex id
def getvertexid(genename):
    return map_str_in_int(genename)


def computehits(geneslist):
    ee, x, y = hits(g)
    sum = 0.0
    for i in x:
        # print(i)
        sum += i
    avg = sum / len(x.get_array())
    # print(avg, avg)
    selected_nodes = {}
    index = 0
    for i in x:
        if i > avg:
            selected_nodes[map_int_in_str.get(index)] = i
            index += 1
    # print('element in x map:', len(x.get_array()))
    print('graph vertices', g.num_vertices())
    print('selected nodes: ', len(selected_nodes))
    # filtering 
    filtered_nodes = {}
    for gene in geneslist:
        if gene in selected_nodes.keys():
            filtered_nodes[gene]=selected_nodes.get(gene)
    return filtered_nodes #selected_nodes

# tiempostart = time.time()
# df = importInteractionDBFromFile("/Users/giuseppeagapito/Desktop/iid.human.2018-04_NoHeader.txt")
# # createNetwork(data_frame=df, genes_target_to_extend=list)
# tiempofine = time.time()
# print('finito tutto in  ', tiempofine - tiempostart)
