import threading as td
import PancLoader as pl
import igraph
from igraph import Graph
import time
import network4SDEG4PA as dg
import sys
import random


def upregulatedgenesselection(dic_up, res_path, data_name, thr, iid_df):
    gu = pl.compute_wilcox(dictionary=dic_up, name=res_path + '/upGraph' + data_name + '.pdf',
                           sign=thr)
    lup, avgup = pl.close_centrality_parallel(gu) 
    ll = dg.create_network_extend_genes_target(
        data_frame=iid_df,
        genes_target_to_extend=lup)
       x = dg.computehits(geneslist=ll)
   


def downregulatedgenesselection(dic_down, res_path, data_name, thr, iid_df):
    gd = pl.compute_wilcox(dictionary=dic_down, name=res_path + '/downGraph' + data_name + '.pdf',
                           sign=thr)
    ld, avgup = pl.close_centrality_parallel(gd)
    ll = dg.create_network_extend_genes_target(
        data_frame=iid_df,
        genes_target_to_extend=ld)
    x = dg.computehits(geneslist=ll)


def sdeg4paParallel():
    statisticalRelevance = 0.005
    logfold = 1.5
    datasetpath = sys.argv[0]
    datasetname = 'GSE-'+str(random.randint(0,9))
    results_path = sys.argv[1]
    print('SEDEG started')
    startTime = time.time()
    dictionario, dic_up, dic_down = pl.load_tsv(datasetpath, logfc=logfold, pvalue=statisticalRelevance,
                                                columns=['Gene.symbol',
                                                         'logFC',
                                                         'P.Value'])  
    iid_dataframe = dg.importInteractionDBFromFile(path=sys.argv[2])
    startuptime = time.time()
    td.Thread(
        upregulatedgenesselection(dic_up=dic_up, res_path=results_path, data_name=datasetname, thr=statisticalRelevance,
                                  iid_df=iid_dataframe)).start()
    enduptime = time.time()
    startdowntime = time.time()
    td.Thread(downregulatedgenesselection(dic_down=dic_down, res_path=results_path, data_name=datasetname,
                                          thr=statisticalRelevance,
                                          iid_df=iid_dataframe)).start()
    enddowntime = time.time()
    end_time = time.time()
    print('up thread time: ', enduptime-startuptime)
    print('down thread time: ', enddowntime-startdowntime)
    print("Total Parallel process ended in ", end_time - startTime)


def main():
    sdeg4paParallel()


if __name__ == '__main__':
    main()
