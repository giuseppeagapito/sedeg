import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from collections import defaultdict
from scipy.stats import wilcoxon as wilcox
from scipy.stats import kruskal as kruskal
import igraph
import csv
import time

"""This methods loads a Gene-expression data in TSV format download from GEO2R. Returns three dictionaries:
The first one contains all the gene meting the threshold, the second one the genes up regulated, and the last one the 
down regulated genes."""


def load_tsv(input, logfc, pvalue, columns):  # update
    dic = defaultdict(list)
    dic_up = defaultdict(list)
    dic_down = defaultdict(list)
    # fields = ['GENE_SYMBOL', 'logFC', 'adj.P.Val']
    fields = columns  # update
    df = pd.read_csv(filepath_or_buffer=input, sep='\t', skipinitialspace=True, usecols=fields, na_filter=True)
    df = df.sort_values(by=[fields[0]])  # by=['GENE_SYMBOL']  #update
    df = df.dropna()

    # filtered_df = df[(abs(df['logFC']) >= logfc) & (df['adj.P.Val'] <= pvalue)]
    filtered_df = df[(abs(df[fields[1]]) >= logfc) & (df[fields[2]] <= pvalue)]  # update
    for index, row in filtered_df.iterrows():
        # print((row.notnull()), ': ', row['GENE_SYMBOL'], '--> ', row['logFC'])
        # dic[row['GENE_SYMBOL']].append(row['logFC'])
        dic[row[fields[0]]].append(row[fields[1]]) #add gene this is the whole dictionary
        # if row['logFC'] > 0.0:
        if row[fields[1]] > 0.0: # found an upregulated gene
           # dic_up[row['GENE_SYMBOL']].append(row['logFC'])
           dic_up[row[fields[0]]].append(row[fields[1]])
        else: #downregulated gene
            # dic_down[row['GENE_SYMBOL']].append(row['logFC'])
            dic_down[row[fields[0]]].append(row[fields[1]])
    print('dictionary length ', len(dic))
    print('dictionary up length ', len(dic_up))
    print('dictionary down length ', len(dic_down))
    # for key, value in dic.items():
    #     print(key, '--->', value, '\n')
    return dic, dic_up, dic_down


def compute_wilcox(dictionary: dict, name: str, sign: float):
    results_list = list()
    keys = dictionary.keys()
    matrix = []
    for k, v in dictionary.items():
        results_list.clear()
        for k2, v2 in dictionary.items():
            w = 0.0
            p = 0.0
            if v != v2:
                w, p = kruskal(list(v), list(v2))
            if p <= sign:
                results_list.append(1) #update
            else:
                results_list.append(0)
        matrix.append(results_list)
    # if len(matrix) > 0 and len(matrix[0]):
    #     print(len(matrix), len(matrix[0]))
    # we convert the similarity matrix in a numpy array 2D
    array = np.array(matrix)

    """iGraph instantiation"""
    """graph creation from the adjacency matrix array """
    g = igraph.Graph.Adjacency(matrix=array)
    """Setting label for the current graph. Label are the key of the dic, N.B. It is mandatory 
    to converted keys set in list"""
    g.vs["label"] = list(keys)
    """" Printing and saving the graph"""
    igraph.plot(g, target=name)
    """Same graph statistics"""
    print("vertices: ", g.vcount())
    print("edges: ", g.ecount())
    print('max degree', g.maxdegree())

    """Iterate the vertices set of the graph and print them"""
    # for d in g.vs:
    #     print('d: ', d, 'degree: ', d.degree())

    """Clique evaluation"""
    # cliques_list = g.maximal_cliques(0, 0, '/Users/giuseppeagapito/cliques.txt')
    """Hub nodes identification"""
    # hub_list = g.hub_score(weights=None, scale=True, return_eigenvalue=False)
    # for i in hub_list:
    #     print(i)
    return g


def write_dictionary_to_file(dictionary: dict, output: str):
    wrt = csv.writer(open(output, "w"))
    for key, val in dictionary.items():
        wrt.writerow([key, val])


def node_degree_on_file(graph: igraph.Graph, output: str):
    wrt = csv.writer(open(output, "w"))
    for d in graph.vs:
        wrt.writerow([d['label'], d.degree(), d.strength()])
        # print('d: ', d, 'degree: ', d.degree())
        # print('d: ', d, 'strength: ', d.strength())
    # commnities = graph.community_edge_betweenness(directed=False, weights=None)
    # print(commnities)
    # print(graph.vs['label'])


def adj_vertices(graph: igraph.Graph, output: str):
    wrt = csv.writer(open(output, "w"))
    for v in graph.vs:
        # res = adjacent_vertices(graph, v)
        # wrt.writerow(str(v['label']))
        ris = graph.neighbors(v)
        for n in ris:
            wrt.writerow([v['label'], graph.vs[n]['label']])
        # print(v['label'], graph.neighbors(v))
        # for i in res:
        #     wrt.writerow(v['label'], i)

# returns the compute CC values for each node
def close_centrality(graph: igraph.Graph, output: str):
    paths = list()
    names = graph.vs['label']
    for i in graph.vs:
        paths.append(graph.get_all_shortest_paths(v=i))  # v=None)
    # g.vs takes the nodes in the entire graph as a list, corresponding to the parameter name
    cc = 0
    cc_avg = 0.0
    with open(output, "w") as wrt:
        # wrt = csv.writer(open(output, "w"))
        ccvs = []
        for p in paths:
            for j in p:
                # print([names[x] for x in j])
                cc += len(j) - 1
            # print('closeness centrality=%s' % ((len(paths) - 1) / cc))
        for j in zip(graph.vs, graph.closeness()):
            ccvs.append({'name': j[0]['label'], 'cc': j[1]})
        pgvs = sorted(ccvs, key=lambda k: k['cc'], reverse=True)  # [:100]
        for i in pgvs:
            # print(i['name'], str(i['cc']))
            wrt.write(i['name'])
            wrt.write('\t')
            wrt.write(str(i['cc']))
            wrt.write('\n')
            cc_avg += (i['cc'])
    print('CC avg: ', cc_avg/len(pgvs))
    return pgvs, cc_avg/len(pgvs)


def median_centrality(g: igraph.Graph, output: str):
    sp = []
    # wrt = csv.writer(open(output, "w"))
    with open(output,'w') as wrt:
    # names = g.vs['label']
        for target in g.vs:
            for v in g.vs:
                print(v, v['label'])
                paths = g.get_all_shortest_paths(v)
                for p in paths:
                    if (target in p and target != p[0] and target != p[-1]):
                        # print(target, p)
                        sp.append(p)
            spbt = 0
            tu = []
            # Deduplication, i to j and j to i are the same path
            for x in sp:
                if (set((x[0], x[-1]))) not in tu:
                    tu.append(set((x[0], x[-1])))
                    spbt += 1
            # print(tu)
            # print('betweenness=%s' % spbt)
            # --------------------------
        # btvs = []
        for p in zip(g.vs, g.betweenness()):
            print(p[0]['label'], ' >>>>>>>>....', p[1])
            wrt.write(p[0]['label'])
            wrt.write('\t')
            wrt.write(str(p[1]))
            wrt.write('\n')
            # btvs.append({'name': p[0]['label'], 'bt': p[1]})
        # pgvs = sorted(btvs, key=lambda k: k['bt'], reverse=True)
        # for i in pgvs:
        #     # print(p[0]['label'],' >>>>>>>>....', p[1])
        #     wrt.writerow(i)
        #     wrt.writerow('\n')


def gene_intersection(dictionary1: dict, dictionary2: dict):
    set1 = set(dictionary1.keys())
    set2 = set(dictionary2.keys())
    intersection = set1.intersection(set2)
    print("intersection\n")
    print(intersection)




def compute_wilcox_parallel(dictionary: dict, name: str, graph):
    results_list = list()
    keys = dictionary.keys()
    matrix = []
    for k, v in dictionary.items():
        results_list.clear()
        for k2, v2 in dictionary.items():
            w, p = wilcox(list(v) + list(v2))
            if p <= 0.005:
                results_list.append(1)
            else:
                results_list.append(0)
        matrix.append(results_list)
    # print(matrix)
    print(len(matrix), len(matrix[0]))
    # we convert the similarity matrix in a numpy array 2D
    array = np.array(matrix)

    """iGraph instantiation"""
    """graph creation from the adjacency matrix array """
    graph = igraph.Graph.Adjacency(matrix=array)
    """Setting label for the current graph. Label are the key of the dic, N.B. It is mandatory 
    to converted keys set in list"""
    graph.vs["label"] = list(keys)
    """" Printing and saving the graph"""
    igraph.plot(graph, target=name)
    """Same graph statistics"""
    print("vertices: ", graph.vcount())
    print("edges: ", graph.ecount())
    print('max degree', graph.maxdegree())

    """Iterate the vertices set of the graph and print them"""
    # for d in g.vs:
    #     print('d: ', d, 'degree: ', d.degree())

    """Clique evaluation"""
    # cliques_list = g.maximal_cliques(0, 0, '/Users/giuseppeagapito/cliques.txt')
    """Hub nodes identification"""
    # hub_list = g.hub_score(weights=None, scale=True, return_eigenvalue=False)
    # for i in hub_list:
    #     print(i)
    # return graph


# returns the compute CC values for each node
def close_centrality_parallel(graph: igraph.Graph):
    paths = list()
    names = graph.vs['label']
    for i in graph.vs:
        paths.append(graph.get_all_shortest_paths(v=i))  # v=None)
    # g.vs takes the nodes in the entire graph as a list, corresponding to the parameter name
    cc = 0
    cc_avg = 0.0
    ccvs = []
    for p in paths:
        for j in p:
            # print([names[x] for x in j])
            cc += len(j) - 1
        # print('closeness centrality=%s' % ((len(paths) - 1) / cc))
    for j in zip(graph.vs, graph.closeness()):
        ccvs.append({'name': j[0]['label'], 'cc': j[1]})
    pgvs = sorted(ccvs, key=lambda k: k['cc'], reverse=True)  # [:100]
    for i in pgvs:
        cc_avg += (i['cc'])
    return pgvs, cc_avg/len(pgvs)

